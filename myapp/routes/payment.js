var config = require('../config').Config; 

var postmark = require("postmark")(config.mailKey);

var mysql = require('mysql');
var connection;
reconnect();
function reconnect()
{
	console.log('Trying to connect sql. in payment.js');
	connection = mysql.createConnection({
    host     : 'localhost',
    user     : 'root',
      password : config.dbPassword,
      database : config.dbName
  	});  	
  	connection.connect(function(err)
	{
		if(err)
		{
			console.log("failed");
			reconnect();
		}
		else
		{
			console.log("connected");
			connection.on('error', function(err) 
			{
			   /* if (!err.fatal) {
			      return;
			    }*/
				if (err.code !== 'PROTOCOL_CONNECTION_LOST') {
			      //throw err;
			      console.log("Error is coontinous. and can not be solved.");
			    }
			    else
			    {
			    	console.log("Retrying!!unexpected connection lost.");
			    	reconnect();
			    }    
			});

		}

	});	    
}

var hash = require('node_hash');


exports.pay=function(req, res){
	
	if(!req.session.user){
		res.render('login',{locals: {title: "Login"}, msg: "You must login before making payement", redir: "/plans"});
	}	
	else	
	{
		var date = new Date();
		date.setTime(date.getTime()+config.timeZone*60*60*1000);  //timestamp		
		var reg_on = date.toUTCString();
		var reg_by = req.session.user.email;			
		var no_users=req.body.num;
		var inr = no_users*11.11;
		var validity=0;			
		var users = Array();
		users[0] = req.body.reg1;users[1] = req.body.reg2;users[2] = req.body.reg3;users[3] = req.body.reg4;users[4] = req.body.reg5;	
		
		var sqltest ="SELECT * FROM users"+" WHERE email = "+connection.escape(users[0])+" OR "+
				"email = "+connection.escape(users[1])+" OR "+
				"email = "+connection.escape(users[2])+" OR "+
				"email = "+connection.escape(users[3])+" OR "+
				"email = "+connection.escape(users[4]);
				
		connection.query(sqltest,function(err, result){								
			console.log("value is"+result[no_users-1]);
			if(result[no_users-1]==undefined){
				res.render('plans',{locals: {title: "Plans", login: req.session.user,  msg: "One of the email ID you entered is either false or not registered with us. Please use Identify button to check all email addresses",image: "users/"+req.session.user.email+".jpg", email: req.session.user.email }});
			}
			else
			{     //all emails are present register them...yeah
				connection.query('INSERT INTO games SET ?', {registered_on: reg_on,
						     registered_by: reg_by,
						     no_of_users: no_users,
						     amount: inr,
						     valid: validity, 
						     started_on: "Not started yet",
						     started_by: "N.A.",
						     started_string: "Not started yet",
						     ended_at: " N.A.",
						     status: "Ready"},
	       			function(err, result) {
																			
					var insertid=result.insertId;						
					console.log("insert ID is"+insertid);
					for(i=0;i<no_users;i++){
						connection.query('INSERT INTO relation SET ?', {gameid: insertid, email: users[i]},function(err, result){ if(err){console.log(err);}});
					}// inserting all users in relation table	
					
					//all database done so go to payement gateway or check coupon code						
					
					var sql_coupon ="SELECT * FROM coupons"+" WHERE valid > 0 AND code = "+connection.escape(req.body.coup);
					connection.query(sql_coupon,function(err, result){
						if(err){console.log(err);}
						
						if(result[0]!=undefined)
						{
							if(result[0].code=="a1pdk"){
								var updatevalid=result[0].valid;
							}
							else{
								var updatevalid=result[0].valid-1;   //decrement valid
							}
							
							
							var sql= "UPDATE games SET valid=1, payid="+connection.escape(result[0].couponid)+" WHERE gameid="+connection.escape(insertid);
							if(req.body.num<=result[0].no_of_users)
							{
								connection.query("UPDATE coupons SET usedBy="+connection.escape(result[0].usedBy+", "+req.session.user.email)+", valid="+connection.escape(updatevalid)+" WHERE code="+connection.escape(result[0].code),function(err, result){
									if(err){console.log(err);}
								});
								connection.query(sql, function(err, results) {	
									if(err){console.log(err);}								
									res.redirect('mycelebrations');
								});
							}
							else
							{								
								var amt =inr-result[0].no_of_users*11.11;
								console.log("amount is being adjusted for"+result[0].no_of_users+" user");
								var obj = {couponid: result[0].couponid, gameid: insertid, isCoupon: "yes", user: req.session.user};
								res.render('processpayment', {locals: {returnfail: "http://ecelebrate.net/statusfail", returnsuccess: "http://ecelebrate.net/status", user: req.session.user, ipnurl: "http://ecelebrate.net/paypal", address: config.paymentAction, amount: amt, information: JSON.stringify(obj), recEmail: config.recEmail}});	
							}
							
						}	
						else
						{							
							var obj = {gameid: insertid, isCoupon: "no", user: req.session.user};
							res.render('processpayment', {locals: {returnfail: "http://ecelebrate.net/statusfail", returnsuccess: "http://ecelebrate.net/status", user: req.session.user, ipnurl: "http://ecelebrate.net/paypal", address: config.paymentAction, amount: inr, information: JSON.stringify(obj), recEmail: config.recEmail}});	
						}
					});
					//res.redirect('mycelebrations');																		
				});//db to insert game 													
			}  //else for all emails entered are right
		});  //sql test
				
		//inserting game in database
	}//session else
};

exports.statusfail=function(req, res){
	smsg= 'Sorry! Transaction you attempted was unable to complete. Please try again later or use a proper card or banking credentials. You will be redirected in 4 seconds.';
	res.render('status',{locals: {status: "failed", flashmsg: smsg}});
};

exports.status=function(req, res){
	smsg= 'You have successfully registered celebration. Your transaction id is: '+req.query.tx;
	res.render('status',{locals: {status: "Success", flashmsg: smsg}});
};

exports.paypal=function(req, res){
	
	var ipn = require('paypal-ipn');
	console.log(" request without stamp has arrived from paypal gateway for celeb id: "+JSON.parse(req.body.custom).gameid);		
	ipn.verify(req.body, function callback(err, msg) 
	{
	    if (err) 
	    {
	      console.log("Error:"+err);
	    }
	    else 
	    {
	    	var info = JSON.parse(req.body.custom);
	    	console.log("paypal has replied. for celeb id: "+info.gameid+"  and email is "+info.user.email);
	        //Do stuff with original params here
	        var stat = req.body.payment_status;
	       	console.log("req.body.payment_status :"+stat+" msg: "+msg);
	       	res.end();
	        if (stat == 'Completed') 
	        {	        	
	        	var payid = req.body.txn_id;
	        	if(info.isCoupon=='yes')
	        	{	        		
	        		console.log("coupon adjustment transaction with coupon id="+info.couponid);
	        		connection.query("UPDATE coupons SET usedBy="+connection.escape(info.user.email)+", valid=0  WHERE couponid="+connection.escape(info.couponid),function(err, result){
	        			if(err)
	        				{console.log(err);}
	        			console.log("coupon used");
	        		});
	        	}
	        	var sql= "UPDATE games SET valid=1, payid="+connection.escape(payid)+" WHERE gameid="+connection.escape(info.gameid);
	        	connection.query(sql, function(err, results) {	
	        		if(err){console.log(err);}	  
	        		console.log("celebration activated.");      		
	        	});

	        	postmark.send({
	        		"From": config.siteEmail, 
	        		"To": info.user.email, 
	        		"Subject": "Your Payment Recieved", 
	        		"HtmlBody": "Dear "+info.user.firstname+",<br><br> We have recieved your payment of <strong>$ "+req.body.payment_gross+"</strong>  You are now ready to celebrate. Please wait for your friends in case, you have chosen group celebration.<br><br><br> regards, <br> Team "+config.appURL
	        	}, function (err, to) {
	        		if (err) 
	        		{
	        			console.log(err);
	        			return;
	        		}
	        		console.log("Email sent for payment to: %s", to);
	        	});		
	        }	
	   	}
	});   //ipn	
};





///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

exports.payforgift=function(req,res){

	var date = new Date();
	date.setTime(date.getTime()+config.timeZone*60*60*1000);  //timestamp
	var gifted_on = date.toUTCString();
	var gift_by = req.session.user.email;

	var rec = req.body.rec;
	var hour=req.body.hour;

	var amt = hour*11.11;
	var validity=0;
	//var coupon = (10000000).toString(36);
	connection.query('INSERT INTO coupons SET ?', {
		code: "xyz",
		valid: validity,
		no_of_users: hour,
		giftedby: gift_by,
		giftedto: rec,
		giftedon: gifted_on
		},
		function(err, result) {
			console.log("code entery done with id ="+result.insertId);
			var couponcod =hash.md5(String(result.insertId));
			couponcod =  couponcod.substring(0,6);
			var sql= "UPDATE coupons SET code= "+connection.escape(couponcod)+"WHERE couponid="+connection.escape(result.insertId);
			var obj = {couponid: result.insertId, isCoupon: "yes", couponcode: couponcod, user: req.session.user};
			connection.query(sql, function(err, results) {
					if(err){console.log(err);}
					console.log("Code generated.");
					res.render('processpayment', {locals: {returnsuccess: "http://ecelebrate.net/statusgift", returnfail: "http://ecelebrate.net/statusgiftfail", user: req.session.user, ipnurl: "http://ecelebrate.net/newgift", address: config.paymentAction, amount: amt, information: JSON.stringify(obj), recEmail: config.recEmail}});
			});
		});
};

exports.statusgiftfail=function(req,res){
	res.render('statusgift',{locals: {status: "failed", flashmsg: "Transaction has been cancelled.",coupon: "", flashmsg2: ""}});
};

exports.statusgift=function(req,res){
	var info = JSON.parse(req.query.cm);	
	smsg2= " Gift coupon is also sent to the reciept and sender for the records. In case, you don't recieve email with in 3 minutes, please contact support department. ";		
	res.render('statusgift',{locals: {status: "Success", flashmsg: "Your transaction id is: "+req.query.tx+" and your couponcode is ", coupon: info.couponcode, flashmsg2: smsg2}});
};

exports.newgift=function(req,res){
	var ipn = require('paypal-ipn');
	console.log(" request without stamp has arrived from paypal gateway for celeb id: "+JSON.parse(req.body.custom).couponid);
	ipn.verify(req.body, function callback(err, msg)
	{
	    if (err)
	    {
	      console.log("Error:"+err);
	    }
	    else
	    {
	    	var info = JSON.parse(req.body.custom);
	    	console.log("paypal has replied. for coupon id: "+info.couponid+"  and email is "+info.user.email);
	        var stat = req.body.payment_status;
	       	console.log("req.body.payment_status :"+stat+" msg: "+msg);
	       	res.end();
	        if (stat == 'Completed')
	        {
	        	var payid = req.body.txn_id;
	        	console.log("coupon id="+info.couponid);
	        	connection.query("UPDATE coupons SET paymentid ="+connection.escape(payid)+", valid=1  WHERE couponid="+connection.escape(info.couponid),function(err, result){
	        		if(err)
	        			{console.log(err);}
	        		console.log("coupon activated");
	        	});	

	        	connection.query("SELECT * FROM coupons WHERE couponid="+connection.escape(info.couponid),function(err, result){
	        		if(err)
	        			{console.log(err);}
	        		else
	        		{
	        			postmark.send({
	        			    "From": config.siteEmail, 
	        			    "To": result[0].giftedto, 
	        			    "Subject": "Gift coupon for "+config.appURL, 			    
	        			    "HtmlBody": "Sir / Ma’am,<br><br>Mr. / Mrs. "+info.user.firstname+" has sent a gift coupon for "+result[0].no_of_users+" user(s) for enjoying, watching and burning of crackers / fireworks.<br><br>Log on to "+config.appURL+" with coupon code <font color=red>"+info.couponcode+"</font>.<br><br>Have a great day !!!!!!!!!!!!!!<br><br><br> regards, <br> Team<br>"+config.appURL,
	        				}, function (err, to) {
	        				    if (err) 
	        				    {
	        				        console.log(err);
	        					    return;
	        					}
	        			 	   console.log("Email sent to"+result[0].giftedto+" who is a reciever.");
	        			});		

	        			postmark.send({
	        						    "From": config.siteEmail, 
	        						    "To": info.user.email, 
	        						    "Subject": "Gift coupon for "+config.appURL, 			    
	        						    "HtmlBody": "Mr. / Mrs. "+info.user.firstname+" <br><br>A gift voucher for "+result[0].no_of_users+" user(s) has been sent to "+result[0].giftedto+" with coupon code <font color=red>"+info.couponcode+"</font>.<br><br>Have a great day !!!!!!!!!!!!!!<br><br><br> Thanks and regards, <br> Team<br>"+config.appURL,
	        							}, function (err, to) {
	        								    if (err) {
	        								        console.log(err);
	        								        return;
	        						 	   }
	        						 	   console.log("Email sent to"+info.user.email+" who is a sender.");
	        					});

	        		}

	        	});
        	}
        		       
	   	}
	});   //ipn		
};


