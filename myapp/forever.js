var forever = require('forever-monitor');

  var child = new (forever.Monitor)('app.js', {
    max: 50,
    //silent: true,
    options: [],    
    outFile: 'out.log', // Path to log output from child stdout
    errFile: 'err.log'  // Path to log output from child stderr
    
  });

  child.on('exit', function () {
    console.log('ecelebrate.js has exited after 50 restarts');
  });

  child.start();