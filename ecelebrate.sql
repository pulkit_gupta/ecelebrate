-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 10, 2012 at 10:55 AM
-- Server version: 5.5.8
-- PHP Version: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `newyear`
--

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE IF NOT EXISTS `coupons` (
  `couponid` int(11) NOT NULL AUTO_INCREMENT,
  `code` text NOT NULL,
  `valid` int(11) NOT NULL,
  `no_of_users` int(11) NOT NULL,
  `usedBy` text NOT NULL,
  `giftedby` text NOT NULL,
  `giftedto` text NOT NULL,
  `giftedon` text NOT NULL,
  `paymentid` text NOT NULL,
  PRIMARY KEY (`couponid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

--
-- Dumping data for table `coupons`
--

INSERT INTO `coupons` (`couponid`, `code`, `valid`, `no_of_users`, `usedBy`, `giftedby`, `giftedto`, `giftedon`, `paymentid`) VALUES
(1, 'a1pdk', 1, 0, ', pulkit.itp@gmail.com', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `games`
--

CREATE TABLE IF NOT EXISTS `games` (
  `gameid` int(11) NOT NULL AUTO_INCREMENT,
  `registered_on` text NOT NULL,
  `registered_by` text NOT NULL,
  `no_of_users` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `valid` tinyint(1) NOT NULL DEFAULT '0',
  `started_on` text NOT NULL,
  `started_string` text NOT NULL,
  `started_by` text NOT NULL,
  `ended_at` text NOT NULL,
  `status` text NOT NULL,
  `payid` text NOT NULL,
  PRIMARY KEY (`gameid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `games`
--

INSERT INTO `games` (`gameid`, `registered_on`, `registered_by`, `no_of_users`, `amount`, `valid`, `started_on`, `started_string`, `started_by`, `ended_at`, `status`, `payid`) VALUES
(1, 'Mon, 10 Dec 2012 16:24:46 GMT', 'pulkit.itp@gmail.com', 1, 111, 1, '1355156689876', 'Mon, 10 Dec 2012 16:24:49 GMT', 'pulkit.itp@gmail.com', ' N.A.', 'Running', '');

-- --------------------------------------------------------

--
-- Table structure for table `relation`
--

CREATE TABLE IF NOT EXISTS `relation` (
  `relationId` int(11) NOT NULL AUTO_INCREMENT,
  `gameid` int(11) NOT NULL,
  `email` text NOT NULL,
  PRIMARY KEY (`relationId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `relation`
--

INSERT INTO `relation` (`relationId`, `gameid`, `email`) VALUES
(1, 1, 'pulkit.itp@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` text,
  `lastname` text,
  `street` text,
  `address2` text,
  `city` text,
  `state` text,
  `postal` text,
  `country` text NOT NULL,
  `email` varchar(200) NOT NULL,
  `mobile` text NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `passwordreset` text NOT NULL,
  `password` text NOT NULL,
  `timestamp` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `firstname`, `lastname`, `street`, `address2`, `city`, `state`, `postal`, `country`, `email`, `mobile`, `active`, `passwordreset`, `password`, `timestamp`) VALUES
(1, 'Pulkit', 'Gupta', '', '', '', '', '', '', 'pulkit.itp@gmail.com', '919958140688', 1, '219c00481c2150a09a750eaecc15ac6c', 'b43911cd0a9fcaf23840e1a0726cf1e8', '1355156649808');
