

/*
 * GET users listing.
 */
var config = require('../config').Config; 

var postmark = require("postmark")(config.mailKey);

var mysql = require('mysql');
var connection;
reconnect();
function reconnect()
{
	console.log('Trying to connect sql. in Member.js');
	connection = mysql.createConnection({
    host     : 'localhost',
    user     : 'root',
      password : config.dbPassword,
      database : config.dbName
  	});  	
  	connection.connect(function(err)
	{
		if(err)
		{
			console.log("failed");
			reconnect();
		}
		else
		{
			console.log("connected");
			connection.on('error', function(err) 
			{
			   /* if (!err.fatal) {
			      return;
			    }*/
				if (err.code !== 'PROTOCOL_CONNECTION_LOST') {
			      //throw err;
			      console.log("Error is coontinous. and can not be solved.");
			    }
			    else
			    {
			    	console.log("Retrying!!unexpected connection lost.");
			    	reconnect();
			    }    
			});

		}

	});	    
}

var hash = require('node_hash');	


exports.page = function(req, res){
	res.render('member', { locals: {login: req.session.user, title: "Home"}, session: req.session.user });
};


exports.settings = function(req, res){
	res.render('edituser', {locals:{login: req.session.user}, session: req.session.user });
};

exports.forgot = function(req, res){
	res.render('forgot',{locals: {title: "Login"},  msg: ""});
};


exports.forgotprocess = function(req, res){

	var email1 = req.body.email;	
	var sql    = 'SELECT * FROM users WHERE email='+connection.escape(email1);
	connection.query(sql, function(err, results) {
		if(err)
		console.log(err);		
		
		if(results[0]!=undefined)
		{							
			var resethash = results[0].passwordreset;
			var sql= 'UPDATE users SET active=0 WHERE email='+connection.escape(email1);
			connection.query(sql, function(err, results) {	
				if (err) 
				{//throw err;
				}
			});
			
			/* var email = require("mailer");
			email.send({
				host : "smtp.gmail.com", // smtp server hostname
				port : "465", // smtp server port
				ssl: true,	 // for SSL support - REQUIRES NODE v0.3.x OR HIGHER
				domain : "smtp.gmail.com", // domain used by client to identify itself to server
				to : email1,
				from : "eutsava@gmail.com",
				subject : "Password reset-link",
				html: "click here to reset your account password http://"+config.appURL+"/resetpassword?q="+resethash+"   or paste it in URL",
				authentication : "login", // auth login is supported; anything else is no auth
				username : "eutsava@gmail.com", // username
				password : "efest.in" // password
				},
				function(err, result){
					if(err){ console.log(err);
					
					}
				}
			);//send email	*/
			
			
			postmark.send({
			    "From": config.siteEmail, 
			    "To": email1, 
			    "Subject": "Password reset-link", 
			    "HtmlBody": "Dear User,<br><br> Please click here to reset your account password http://"+config.appURL+"/resetpassword?q="+resethash+"   or paste it in URL<br><br><br>regards, <br> Team-"+config.appURL
				}, function (err, to) {
					    if (err) {
					        console.log(err);
					        return;
			 	   }
			 	   console.log("Email sent to: %s", to);
			});
			
			
			
			res.render('login', { locals: {title: "Login"}, msg: 'Reset password link sent to your registered Email Id', redir: "/member"});
		}
		else
		{
			res.render('login', { locals: {title: "Login"}, msg: 'Email is not registered with us, register it!', redir: "/member"});
		}
	});//sql		
	
};


exports.resetpassword = function(req, res){
	
	var sql= 'SELECT * FROM users WHERE active = 0 AND passwordreset = ' + connection.escape(req.query.q);
	connection.query(sql, function(err, results) {	
			
		if(results[0]==undefined)
		{			
			res.render('login', { locals: {title: "Login"}, msg: 'Link has expired', redir: "/member"});
		}
		
		else
		{
			res.render('resetpassword', {locals:{ title: "Login"}, resetlink: req.query.q});	
		}		
	});

};

exports.newpassword = function(req, res){

	
	var str13 = req.body.password;
	var date = new Date();
	var str14 = date.getTime()-date.getTimezoneOffset() * 60*1000;  //timestamp
	var salt=String(str14);
        str13 = hash.md5(str13, salt);	               
	var sql= "UPDATE users SET active=1, password="+connection.escape(str13)+", timestamp="+connection.escape(str14)+" WHERE active = 0 AND passwordreset ="+connection.escape(req.body.resetlink);
	
	connection.query(sql, function(err, results) {	
	
		res.render('login', { redir: "member", msg: 'Your password is changed successfully. Please Login'});
	});

};

exports.gift=function(req,res){
	res.render('giftpay', {locals: {title: "Plans", login: req.session.user, msg: ""}});
};



exports.plans=function(req,res){	
	var photo="users/user.jpg", email1="";
	console.log("session is not there");
	if(req.session.user){
		console.log("session is there");
		photo= "users/"+req.session.user.email+".jpg";
		email1=req.session.user.email;							
	}	
	res.render('plans', {locals: {title: "Plans",login: req.session.user, email: email1, image: photo, msg: ""}});					
};

exports.mycelebrations=function(req, res){
	var sql="SELECT gameid FROM relation WHERE email="+connection.escape(req.session.user.email);
	connection.query(sql, function(err, result){
		if(result.length<=0){
			//no games found or registered
			//res.render('mycelebrations',{msg: "No Celebration session has been registered"});
			console.log("no celebration found");
			res.render('plans',{locals:{ title: "Plans", login: req.session.user,  image: "users/"+req.session.user.email+".jpg", email: req.session.user.email, msg: "No Celebration session has been registered, Please register it here."}});
		}
		else{
			var games=Array(), users = Array(), checker= Array();  			
			var flag=result.length;			
			
			for(i=0;i<result.length;i++){												
				var q1 = connection.query("SELECT * FROM games WHERE valid=1 AND gameid="+connection.escape(result[i].gameid),function(err, rs){																																							
						if(rs[0]!=undefined)
						{checker.push(rs[0]);}
						games.push(rs[0]);																		
						flag=games.length;					
					
				});
				
				var q2 = connection.query("SELECT email FROM relation WHERE gameid="+connection.escape(result[i].gameid),function(err, re2){					
					var temarr = Array();					
					var ind=0;
					for(j=0;j<re2.length;j++){
						if(re2[j].email!=req.session.user.email)
						{
							temarr[ind]= re2[j].email;														
							ind++;
						}	
					}	
					for(k=ind;k<4;k++){
						temarr[k]="N.A.";
					}
					users.push(temarr);					
																		
				});
			}//for loop get details of all games associated with email of session
						
			
			var q1status=0, q2status=0;			
  			q2.on('end', function() {
  				q2status=1;
  				if(q1status==1){
  					
			    		console.log("all rows for users have been received");
			    		console.log("value of first game"+checker[0]);
			    		
			    		if(checker[0]==undefined)
			    		res.render('plans',{locals:{ title: "Plans", login: req.session.user,  image: "users/"+req.session.user.email+".jpg", email: req.session.user.email, msg: "No Celebration session has been registered, Please register it here."}});			    					    			    
			    		else
			    		res.render("mycelebrations",{locals: {login: req.session.user, obj: JSON.stringify({rows:flag, game: games, user:users})}});						    		
			    		
			    		
			    	}
			    	else
			    	{console.log("waiting for games to get over");}
						    
  			});
  			q1.on('end', function() {
  				q1status=1;
  			    if(q2status==1){
			    	console.log("all rows for games have been received");			    	
			    	
					if(checker[0]==undefined)
			    		res.render('plans',{locals:{ title: "Plans", login: req.session.user, image: "users/"+req.session.user.email+".jpg", email: req.session.user.email, msg: "No Celebration session has been registered, Please register it here."}});			    					    			    
			    		else
			    		res.render("mycelebrations",{locals: {login: req.session.user, obj: JSON.stringify({rows:flag, game: games, user:users})}});						    		
			    }
			    else
			    {console.log("Waiting for users to get over");}
						    
  			});
		}//else
	});	
};


exports.play=function(req, res){	

	if(!req.session.user)
	{
		res.redirect('/login');
	}
	else
	{
		var gameid=req.query.q;
		//condition of validity of gameid from database
		var sql= 'SELECT email FROM relation WHERE email = '+connection.escape(req.session.user.email)+'AND gameid = ' + connection.escape(gameid);		
		connection.query(sql, function(err, results) 
		{			
			if(results.length==1)
			{					
				var sql1= 'SELECT * FROM games WHERE gameid= '+connection.escape(gameid)+'AND valid=1 AND (status="Ready" OR status="Running") ';
				connection.query(sql1,function(err, result1){
					if(result1.length==1)
					{	
						if(result1[0].started_on=="Not started yet")
						{
						
							console.log("game is new");
							var date = new Date();
							var time = date.getTime()- date.getTimezoneOffset()*60*1000;  //timestamp to update in database
							var date2 = new Date(date.getTime()+config.timeZone*60*60*1000);
							var  time_string = date2.toUTCString();
							
							var sql2='UPDATE games SET status="Running",started_string='+connection.escape(time_string)+', started_on='+connection.escape(time)+', started_by='+connection.escape(req.session.user.email)+' WHERE gameid='+connection.escape(gameid);
							connection.query(sql2, function(err, result2){
								//give whole one hour			
								
								if(!err)
								res.render('play',{locals: JSON.stringify({name: req.session.user.firstname, email: req.session.user.email, gameid: req.query.q, timersec: 3600})});
							});
						
						}
						else
						{  console.log("game is being resumed");
							var date = new Date();
							var time = date.getTime()/1000 - date.getTimezoneOffset() * 60;  //timestamp to update in database
							var msdiff = 3600+result1[0].started_on/1000-time;
							res.render('play',{locals: JSON.stringify({name: req.session.user.firstname, email: req.session.user.email, gameid: req.query.q, timersec: Math.round(msdiff)})});
						}
					}
					else
					{
					
						//time has over write valid=0
						res.render('plans',{locals:{ title: "Plans", login: req.session.user,  image: "users/"+req.session.user.email+".jpg", email: req.session.user.email, msg: "Your Celebration has over. You can register again"}});
					}	
					
				});
			
			}
			else
			{			
				res.render('plans',{locals:{image: "users/"+req.session.user.email+".jpg", email: req.session.user.email, msg: "You don't have any registered Celebration.Please register"}});
			}
		});	
	}	
};	