
var development = {
  appURL : 'localhost',
  timeZone: 5.5,
  socketPort : 80,
  dbName: 'newyear',
  dbPassword: '',
    
  mailKey: "7cd41bb1-8d41-4968-912e-0d001117e6eb",
  siteEmail: "info@ecelebrate.net",

  paymentAction: "https://sandbox.paypal.com/cgi-bin/webscr",
  recEmail: "pulkit_1355586581_biz@gmail.com",
    
  env : global.process.env.NODE_ENV || 'development'
};


var production = {
  appURL : 'www.ecelebrate.net',
  timeZone: 5.5,
  socketPort : 4000,  
  dbName: 'newyear',
  dbPassword: 'pulkit$2212',
  
  mailKey: "7cd41bb1-8d41-4968-912e-0d001117e6eb",
  siteEmail: "info@ecelebrate.net",
  paymentAction: "https://www.paypal.com/cgi-bin/webscr",
  recEmail: "compliance@ecelebrate.net",
//paymentAction: "https://sandbox.paypal.com/cgi-bin/webscr",
 //recEmail: "pulkit_1355586581_biz@gmail.com",

  
  env : global.process.env.NODE_ENV || 'development'
};

exports.Config = global.process.env.NODE_ENV === 'production' ? production : development;